export enum GRADES {
  EIGHTH = '8th',
  NINTH = '9th',
  TENTH = '10th',
  ELEVENTH = '11th',
  TWELFTH = '12th',
  COLLEGE = 'College',
  OTHER = 'Other',
}

export type QuillTypes =
  | 'bold'
  | 'italic'
  | 'stroke'
  | 'underline'
  | 'color'
  | 'background'

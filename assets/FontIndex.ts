export const FontIndex = {
  WorkSans: {
    Regular: 'WorkSans',
    Bold: 'WorkSansBold',
    Medium: 'WorkSansMedium',
  },
}

export const ImageIndex = {
  logo: require('./images/logo_white.png'),
  welcome: require('./images/welcome.png'),
  avatars: {
    chatbot: require('./images/avatars/avatar_chatbot.png'),
    student: require('./images/avatars/avatar_profile.png'),
    volunteer: require('./images/avatars/avatar_volunteer.png'),
    profile: require('./images/avatars/avatar_profile.png'),
  },
  icons: {
    chatEditor: require('./images/icons/icon_doc_editor.png'),
    whiteBoard: require('./images/icons/icon_whiteboard.png'),
    goBack: require('./images/icons/icon_back.png'),
    check: require('./images/icons/icon_check.png'),
    arrowhead: require('./images/icons/icon_arrowhead.png'),
    toolbar: {
      boldIcon: require('./images/icons/toolbar/toolbar_bold.png'),
      italicIcon: require('./images/icons/toolbar/toolbar_italic.png'),
      underlineIcon: require('./images/icons/toolbar/toolbar_underline.png'),
      strikeIcon: require('./images/icons/toolbar/toolbar_strike.png'),
      colorIcon: require('./images/icons/toolbar/toolbar_color.png'),
      backgroundIcon: require('./images/icons/toolbar/toolbar_background.png'),
    },
  },
  feedback: {
    postFeedback: require('./images/feedback/updog_feedback_successfully.png'),
    goArrow: require('./images/feedback/icon_go_arrow.png'),
  },
  onboarding: {
    allSet: require('./images/onboarding/updog_all_set.png'),
    eligible: require('./images/onboarding/updog_eligible.png'),
    accountVerified: require('./images/onboarding/updog_account_verified.png'),
    notEligible: require('./images/onboarding/updog_not_eligible.png'),
    confetti: require('./images/onboarding/confetti.png'),
    messageSent: require('./images/onboarding/updog_message_sent.png'),
    rules: {
      rule1: require('./images/onboarding/rules/onboarding_rule1.png'),
      rule2: require('./images/onboarding/rules/onboarding_rule2.png'),
      rule3: require('./images/onboarding/rules/onboarding_rule3.png'),
    },
  },
  dashboard: {
    background: require('./images/dashboard/core_background.png'),
    openMenuButton: require('./images/dashboard/icon_menu.png'),
    deleteAccount: require('./images/dashboard/updog_delete_account.png'),
    notificationDog: require('./images/dashboard/updog_notification.png'),

    state: {
      inSession: require('./images/dashboard/state/updog_in_session.png'),
      Waiting: require('./images/dashboard/state/updog_waiting.png'),
      Banned: require('./images/dashboard/state/updog_banned.png'),
    },

    subjects: {
      college: require('./images/dashboard/subjects/subject_college.png'),
      math: require('./images/dashboard/subjects/subject_math.png'),
      readWriting: require('./images/dashboard/subjects/subject_read_write.png'),
      sat: require('./images/dashboard/subjects/subject_sat.png'),
      science: require('./images/dashboard/subjects/subject_science.png'),
      socialStudies: require('./images/dashboard/subjects/subject_social_studies.png'),
    },
  },
}

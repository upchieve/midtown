export const Colors = {
  success: '#16D2AA',
  error: '#F44747',
  warning: '#FF8C5F',
  information: '#1855D1',
  white: '#FFFF',
  backgroundGray: '#F1F3F6',
  backgroundBlue: '#E3F2FD',
  backgroundGreen: '#E9F9F5',

  black: '#000000',
  softBlack: '#343440',
  secondaryGray: '#565961',
  disabledGray: '#ABB2BD',
  defaultGray: '#8B939F',
  borderGray: '#D8DEE6',

  selectedGreen: '#F2FBF9',
  hoverGreen: '#0D8269',
  selectedBorderGreen: '#16D2AA',

  linkBlue: '#0F44FF',
}

export type PrimaryColorType = 'success' | 'error' | 'warning' | 'information'
